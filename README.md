The "clean up" patches for up
=============================

## About

This is a collection of some patches for [`up`](https://github.com/akavel/up), which in particular affects the workflow in the following way:

  - disables the pointless "informational" message at the bottom
  - enables exit via Escape
  - enables exit code 1 on Escape / Ctrl-c
  - disables crap scripts creation (instead, it just prints out the results to stdout)

## Integration

Given the aforesaid changes, the easiest way now to use `up`, is to integrate it like this:

    function pp() {
        arg="$*"
        com=$($arg | up)
        if [ "$?" -eq 0 ] && ! [ -z "$com" ]
        then
            com="$arg | $com"
            printf "$com" | $XCOPY
        fi
    }

...where `$XCOPY` can be defined like this:

    if uname -a | grep -iq darwin
    then
        export XCOPY='/usr/bin/pbcopy'
        export XPASTE='/usr/bin/pbpaste'
    else
        export XCOPY='xclip -selection clipboard'
        export XPASTE='xclip -out -selection clipboard'
    fi
